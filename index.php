    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Capital News</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    <h1>Актуальные новости столицы</h1>
    <section class="news-wrapper">
        <?php
        $doc = new DOMDocument;
        $rss = file_get_contents('https://www.liga.net/news/capital/rss.xml');
        $news = new SimpleXmlElement( $rss );
        foreach ( $news->channel->item as $key => $item ) {
            echo '<div class="news__item">';
            echo '<h3>'.$item->title.'</h3>';
            echo '<img src="'.$item->enclosure['url'].'">';

            echo  '<br><br><a href="'. $item->link . '">Смотреть больше</a><br>';
            echo '<br></div>';}
        ?>
    </section>
    </body>
    </html>

